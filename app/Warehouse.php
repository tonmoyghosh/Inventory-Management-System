<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
     protected $fillable = ['name', 'created_by', 'updated_by'];
     

     // fetch created user data 
     public function create_by()
     {
     	return $this->belongsTo('App\User', 'created_by');
     }
  
     // fetch updated user data
     public function update_by()
     {
     	return $this->belongsTo('App\User', 'updated_by');
     }

     // relation with warehouse table
     public function product()
     {
          return $this->hasMany(Product::class);
     }

}
