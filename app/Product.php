<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'weight', 'quantity', 'category_id', 'warehouse_id', 'created_by', 'updated_by'];
    

     // fetch created user data 
     public function create_by()
     {
     	return $this->belongsTo('App\User', 'created_by');
     }
  
     // fetch updated user data
     public function update_by()
     {
     	return $this->belongsTo('App\User', 'updated_by');
     }
     
     // fetch category name
     public function category()
     {
          return $this->belongsTo(Category::class);
     }

     //fetch warehouse name
     public function warehouse()
     {
          return $this->belongsTo(Warehouse::class);
     }
}
