<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_combination extends Model
{
    
     protected $fillable = ['product_id', 'size_id', 'color_id', 'created_by', 'updated_by'];
}
