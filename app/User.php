<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

 
    // relation with category table 
    public function category()
    {
        return $this->hasMany(Category::class);
    }

    // relation with warehouse table
    public function warehouse()
    {
        return $this->hasMany(Warehouse::class);
    }

    // relation with color table
    public function color()
    {
        return $this->hasMany(Color::class);
    }


    // relation with size table
    public function size()
    {
        return $this->hasMany(Size::class);
    }

    // relation with user role table
    public function user_role()
    {
        return $this->hasMany(User_role::class);
    }

    //relation with product table
    public function product()
    {
        return $this->hasMany(Product::class);
    }

    


}
