<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Size;
use App\User;

class SizeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        // session check
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // fetch all size data
        $sizes = Size::all();

        $serialNo = 1;

        // return size list page
        return view('admin.size.index')->with('sizes', $sizes)
                                       ->with('serialNo', $serialNo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // size create form 

         return view('admin.size.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // size form validation 
         $this->validate(request(), [

            'name' => 'required'

         ]);
        
         // store color data
        Size::create([
           
           'created_by'    => auth()->id(),
           'name'          => request('name')

         ]);

       session()->flash('msg', 'Size Created Sucessfully');

       // redirect to size form page 
       return redirect('create-size');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // fetch size data using id

        $size = Size::find($id);

        // return size update form

        return view('admin.size.edit')->with('size', $size);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // fetch size data using id
        $size = Size::find($id);

        // update the size data
        $size->update([
           
           'updated_by' => auth()->id(),
           'name'       => request('name')

        ]);

        //fetch all size data
        $sizes = Size::all();

        session()->flash('msg', 'Size Updated Sucessfully');
        
        // redirect to the color list
        return redirect('size-list')->with('sizes', $sizes);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         // fetch size data using id
        $size = Size::find($id);

        // delete the size data
        $size->delete();

        //fetch all size data
        $sizes = Size::all();

        session()->flash('msg', 'Size Deleted Sucessfully');

        return redirect('size-list')->with('sizes', $sizes);
    }
}
