<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Color;
use App\Size;
use App\User;
use App\Product_combination;

class CombinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        //fetch all product data
         $products = Product::all('id', 'name');

        // fetch all color data
         $colors = Color::all('id', 'name');

        // fetch all size data
         $sizes = Size::all('id', 'name');

        // return to create product combination form page
        return view('admin.product_combination.create')->with('products', $products)
                                                       ->with('colors', $colors)
                                                       ->with('sizes', $sizes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // product combination form validation 
        $this->validate(request(), [

            'product_id'  => 'required|not_in:0',
            'size_id'     => 'required',
            'color_id'    => 'required'

        ]); 
        
        // store size id in variable
        $size_id  = request('size_id');

        // store color id in variable
        $color_id = request('color_id');

       
        // store product combination data in table
        for ($i=0; $i < count(request('size_id')) ; $i++) { 

            for($j=0; $j < count(request('color_id')) ; $j++) {
            
            Product_combination::create([
           
               'created_by'    => auth()->id(),
               'product_id'    => request('product_id'),
               'size_id'       => $size_id[$i],
               'color_id'      => $color_id[$j]

            ]); 

         } 

       }

        session()->flash('msg', 'Product combination created Sucessfully');

        // redirect to product combination form page 
        return redirect('create-product-combination'); 

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
