<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Warehouse;
use App\User;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        // session check
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // fetch all product data
        $products = Product::all();
        
        $serialNo = 1;

        // return product list page
        return view('admin.product.index')->with('products', $products)
                                           ->with('serialNo', $serialNo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {    
         // fetch all category
         $category = Category::all();

         //fetch all warehouse
         $warehouse = Warehouse::all();

         // product create form 
         return view('admin.product.create')->with('category', $category)
                                            ->with('warehouse', $warehouse);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    

         // product form validation 
         $this->validate(request(), [

            'name'            => 'required',
            'category_id'     =>  'required|not_in:0',
            'warehouse_id'    =>  'required|not_in:0'

         ]);
        
        // store product data
        Product::create([
           
           'created_by'   => auth()->id(),
           'name'         => request('name'),
           'weight'       => request('weight'),
           'quantity'     => request('quantity'),
           'category_id'  => request('category_id'),
           'warehouse_id' => request('warehouse_id')

         ]);

       session()->flash('msg', 'Product Created Sucessfully');

       // redirect to product form page 
       return redirect('create-product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // fetch specific product toltal data
        $product = Product::find($id);

        // return to product data details page
        return view('admin.product.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        // fetch all category
         $category = Category::all();

         //fetch all warehouse
         $warehouse = Warehouse::all();


        // fetch product data using id

        $product = Product::find($id);

        // return product update form

        return view('admin.product.edit')->with('category', $category)
                                         ->with('warehouse', $warehouse)
                                         ->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // fetch product data using id
        $product = Product::find($id);

        // update the product data
        $product->update([
           
           'updated_by'    => auth()->id(),
           'name'          => request('name'),
           'weight'        => request('weight'),
           'quantity'      => request('quantity'),
           'category_id'   => request('category_id'),
           'warehouse_id'  => request('warehouse_id')

        ]);

        //fetch all product data
        $products = Product::all();

        session()->flash('msg', 'Product Updated Sucessfully');
        
        // redirect to the product list
        return redirect('product-list')->with('products', $products);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // fetch product data using id
        $product = Product::find($id);

        // delete the product data
        $product->delete();

        //fetch all product data
        $products = Product::all();

        session()->flash('msg', 'Product Deleted Sucessfully');

        return redirect('product-list')->with('products', $products);
    }
}
