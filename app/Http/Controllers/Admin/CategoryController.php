<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\User;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        // session check
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // fetch all category data
        $categories = Category::all();

        $serialNo = 1;

        // return category list page
        return view('admin.category.index')->with('categories', $categories)
                                           ->with('serialNo', $serialNo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        // category create form 

        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
         // category form validation 
         $this->validate(request(), [

            'name' => 'required'

         ]);

        
         // store category data
         Category::create([
           
           'created_by'    => auth()->id(),
           'name'          => request('name')

         ]);

       session()->flash('msg', 'Category Created Sucessfully');

       // redirect to category form page 
       return redirect('create-category');

     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // fetch category data using id

        $category = Category::find($id);

        // return category update form

        return view('admin.category.edit')->with('category', $category);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        // fetch category data using id
        $category = Category::find($id);

        // update the category data
        $category->update([
           
           'updated_by' => auth()->id(),
           'name'       => request('name')

        ]);

        //fetch all category data
        $categories = Category::all();

        session()->flash('msg', 'Category Updated Sucessfully');
        
        // redirect to the category list
        return redirect('category-list')->with('categories', $categories);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        // fetch category data using id
        $category = Category::find($id);

        // delete the category data
        $category->delete();

        //fetch all category data
        $categories = Category::all();

        session()->flash('msg', 'Category Deleted Sucessfully');

        return redirect('category-list')->with('categories', $categories);
    }
}
