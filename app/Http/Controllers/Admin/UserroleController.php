<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\User_role;
use App\User;

class UserroleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        // session check
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // fetch all user role data
        $user_roles = User_role::all();

        $serialNo = 1;

        // return user role list page
        return view('admin.user_role.index')->with('user_roles', $user_roles)
                                            ->with('serialNo', $serialNo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // create user role form page
        return view('admin.user_role.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // user role form validation 
         $this->validate(request(), [

            'name' => 'required'

         ]);
        
         // store user role data
        User_role::create([
           
           'created_by'    => auth()->id(),
           'name'          => request('name')

         ]);

       session()->flash('msg', 'User role Created Sucessfully');

       // redirect to category form page 
       return redirect('create-user-role');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // fetch user role data using id

        $user_role = User_role::find($id);

        // return user role update form

        return view('admin.user_role.edit')->with('user_role', $user_role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // fetch user role data using id
        $user_role = User_role::find($id);

        // update the user role data
        $user_role->update([
           
           'updated_by' => auth()->id(),
           'name'       => request('name')

        ]);

        //fetch all user role data
        $user_roles = User_role::all();

        session()->flash('msg', 'User role Updated Sucessfully');
        
        // redirect to the uer role list
        return redirect('user-role-list')->with('user_roles', $user_roles);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // fetch user role data using id
        $user_role = User_role::find($id);

        // delete the user role data
        $user_role->delete();

        //fetch all user role data
        $user_roles = User_role::all();

        session()->flash('msg', 'User role Deleted Sucessfully');

        return redirect('user-role-list')->with('user_roles', $user_roles);
    }
}
