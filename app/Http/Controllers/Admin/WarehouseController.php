<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Warehouse;
use App\User;


class WarehouseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        // session check

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         // fetch all warehouse data

         $warehouses = Warehouse::all();

         $serialNo = 1;

         // return category list page

         return view('admin.warehouse.index')->with('warehouses', $warehouses)
                                            ->with('serialNo', $serialNo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // warehouse create form 

        return view('admin.warehouse.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // warehouse form validation 
         $this->validate(request(), [

            'name' => 'required'

         ]);

        
         // store warehouse data
         Warehouse::create([
           
           'created_by'    => auth()->id(),
           'name'          => request('name')

         ]);

       session()->flash('msg', 'Warehouse Created Sucessfully');

       // redirect to warehouse form page 
       return redirect('create-warehouse');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // fetch warehouse data using id

        $warehouse = Warehouse::find($id);

        // return warehouse update form

        return view('admin.warehouse.edit')->with('warehouse', $warehouse);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // fetch warehouse data using id
        $warehouse = Warehouse::find($id);

        // update the warehouse data
        $warehouse->update([
           
           'updated_by' => auth()->id(),
           'name'       => request('name')

        ]);

        //fetch all warehouse data
        $warehouses = Warehouse::all();

        session()->flash('msg', 'Warehouse Updated Sucessfully');
        
        // redirect to the warehouse list
        return redirect('warehouse-list')->with('warehouses', $warehouses);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // fetch category data using id
        $warehouse = Warehouse::find($id);

        // delete the category data
        $warehouse->delete();

        //fetch all category data
        $warehouses = Warehouse::all();

        session()->flash('msg', 'Warehouse Deleted Sucessfully');

        return redirect('warehouse-list')->with('warehouses', $warehouses);
    }
}
