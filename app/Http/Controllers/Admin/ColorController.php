<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Http\Controllers\Controller;
use App\Color;
use App\User;

class ColorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        // session check
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // fetch all color data
        $colors = Color::all();

        $serialNo = 1;

        // return color list page
        return view('admin.color.index')->with('colors', $colors)
                                        ->with('serialNo', $serialNo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         // category create form 

         return view('admin.color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // color form validation 
         $this->validate(request(), [

            'name' => 'required'

         ]);

        
         // store color data
         Color::create([
           
           'created_by'    => auth()->id(),
           'name'          => request('name')

         ]);

       session()->flash('msg', 'Color Created Sucessfully');

       // redirect to color form page 
       return redirect('create-color');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // fetch color data using id

        $color = Color::find($id);

        // return color update form

        return view('admin.color.edit')->with('color', $color);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // fetch color data using id
        $color = Color::find($id);

        // update the color data
        $color->update([
           
           'updated_by' => auth()->id(),
           'name'       => request('name')

        ]);
        
        //fetch all color data
        $colors = Color::all();

        session()->flash('msg', 'Color Updated Sucessfully');
        
        // redirect to the color list
        return redirect('color-list')->with('colors', $colors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // fetch color data using id
        $color = Color::find($id);

        // delete the color data
        $color->delete();

        //fetch all color data
        $colors = Color::all();

        session()->flash('msg', 'Color Deleted Sucessfully');

        return redirect('color-list')->with('colors', $colors);
    }
}
