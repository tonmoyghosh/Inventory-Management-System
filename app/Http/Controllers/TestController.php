<?php

namespace App\Http\Controllers;

use Request;
use App\Size;

class TestController extends Controller
{    

	 public function __construct()
    {   
        // session check
        $this->middleware('auth');
    }


     public function index()
     {
        $data = Size::all();

        return view('test')->with('data', $data);
     }
}
