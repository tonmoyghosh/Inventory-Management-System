<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Redirect Login Page
Route::get('/', function(){

	return redirect('login');

});

// Auth Routing
Auth::routes();

// Dashboard 
Route::get('/home', 'Admin\HomeController@index')->name('home');

// ---- - Admin Panel -------

// Product Category
Route::get('/create-category', 'Admin\CategoryController@create')->name('createCategory');
Route::post('/store-category', 'Admin\CategoryController@store')->name('storeCategory');
Route::get('/category-list', 'Admin\CategoryController@index')->name('listCategory');
Route::get('/category/{id}', 'Admin\CategoryController@edit')->name('editCategory');
Route::post('/update-category/{id}', 'Admin\CategoryController@update')->name('updateCategory');
Route::get('/delete-category/{id}', 'Admin\CategoryController@destroy')->name('deleteCategory');

// Warehouse
Route::get('/create-warehouse', 'Admin\WarehouseController@create')->name('createWarehouse');
Route::post('/store-warehouse', 'Admin\WarehouseController@store')->name('storeWarehouse');
Route::get('/warehouse-list', 'Admin\WarehouseController@index')->name('listWarehouse');
Route::get('/warehouse/{id}', 'Admin\WarehouseController@edit')->name('editWarehouse');
Route::post('/update-warehouse/{id}', 'Admin\WarehouseController@update')->name('updateWarehouse');
Route::get('/delete-warehouse/{id}', 'Admin\WarehouseController@destroy')->name('deleteWarehouse');

// User
Route::get('/user-list', 'Admin\UserController@index')->name('userList');

// User Role
Route::get('/create-user-role', 'Admin\UserroleController@create')->name('createUserRole');
Route::post('/store-user-role', 'Admin\UserroleController@store')->name('storeUserRole');
Route::get('/user-role-list', 'Admin\UserroleController@index')->name('userRoleList');
Route::get('/user-role/{id}', 'Admin\UserroleController@edit')->name('editUserRole');
Route::post('/update-user-role/{id}', 'Admin\UserroleController@update')->name('updateUserRole');
Route::get('delete-user-role/{id}', 'Admin\UserroleController@destroy')->name('deleteUserRole');

// Color
Route::get('/create-color', 'Admin\ColorController@create')->name('createColor');
Route::post('/store-color', 'Admin\ColorController@store')->name('storeColor');
Route::get('/color-list', 'Admin\ColorController@index')->name('colorList');
Route::get('/color/{id}', 'Admin\ColorController@edit')->name('editColor');
Route::post('/update-color/{id}', 'Admin\ColorController@update')->name('updateColor');
Route::get('/delete-color/{id}', 'Admin\ColorController@destroy')->name('deleteColor');

// Size
Route::get('/create-size', 'Admin\SizeController@create')->name('createSize');
Route::post('/store-size', 'Admin\SizeController@store')->name('storeSize');
Route::get('/size-list', 'Admin\SizeController@index')->name('sizeList');
Route::get('/size/{id}', 'Admin\SizeController@edit')->name('editSize');
Route::post('/update-size/{id}', 'Admin\SizeController@update')->name('updateSize');
Route::get('/delete-size/{id}', 'Admin\SizeController@destroy')->name('deleteSize');

// Product 
Route::get('/create-product', 'Admin\ProductController@create')->name('createProduct');
Route::post('/store-product', 'Admin\ProductController@store')->name('storeProduct');
Route::get('/product-list', 'Admin\ProductController@index')->name('productList');
Route::get('/product-details/{id}', 'Admin\ProductController@show')->name('productDetail');
Route::get('/product/{id}', 'Admin\ProductController@edit')->name('editProduct');
Route::post('/update-product/{id}', 'Admin\ProductController@update')->name('updateProduct');
Route::get('/delete-product/{id}', 'Admin\ProductController@destroy')->name('deleteProduct');

// Product Combination 
Route::get('/create-product-combination', 'Admin\CombinationController@create')->name('createProductCombination');
Route::post('/store-product-combination', 'Admin\CombinationController@store')->name('storeProductCombination');


Route::get('/test', 'TestController@index');




