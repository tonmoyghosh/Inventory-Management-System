 @extends('layouts.app')


 @section('title')

      <title>Admin | User List</title>

  @stop

 @section('content')

 <!-- main content start -->
  <div class="content-wrapper">
 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <div class="box">

        <!-- all category data -->
        <br>
        <div class="box-header">
            <h4 style="color: green"> {{Session::get('msg')}} <h4>
            <h3 class="box-title">All user list</h3>
        </div>
           
            
        <div class="box-body">
              
              <table id="example1" class="table table-bordered table-striped">

              <thead>

                <tr>
                  <th>Serial No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Action</th>
                </tr>
                
                </thead>
             
              
              @foreach($users as $data) 

                <tr>
                  <td>{{ $serialNo++ }}</td>
                  <td>{{ $data->name }}</td>
                  <td>{{ $data->email }}</td>
                  <td><a href="#" class="btn btn-warning">Details</a> <a href="#" class="btn btn-danger">Delete</a></td>
                </tr>

             @endforeach
          
        </table>

         </div>


             </div>
            
          </div>
          
        </div>
        
    </section>
    
  </div>

   @stop


