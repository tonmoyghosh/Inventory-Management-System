@extends('layouts.app')


 @section('title')

      <title>Admin | Create Product Combination</title>

  @stop

 @section('content')

  <!-- main content start -->
  <div class="content-wrapper">
 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <div class="box">
            
            <!-- create product combination form -->
          
           <div class="box-header">
               <h4 style="color: green"> {{Session::get('msg')}} <h4>
               <br>
               <h3 class="box-title">Create a new product combination</h3>
           </div>
           
            
      <div class="box-body">
              
          <div class="row">

            
            <div id="root">
              
              <input type="text" name="" v-model="message">

            </div>
          

            <form action="" method="">

                 {{ csrf_field() }}
             
             
               <div class="col-sm-12">
                  <div class="form-group col-xs-12">
                     <input type="submit" class="btn btn-primary" value="Submit">
                  </div>
               </div>


            </form>

          </div>

      </div>

        </div>
            
      </div>
          
   </div>
        
  </section>
    
</div>

<!-- main content close -->

  @stop
