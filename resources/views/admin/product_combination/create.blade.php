@extends('layouts.product_combination')


 @section('title')

      <title>Admin | Create Product Combination</title>

  @stop

 @section('content')

  <!-- main content start -->
  <div class="content-wrapper">

    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <div class="box">
            
            <!-- create product combination form -->
          
           <div class="box-header">
                <h4 style="color: green"> {{Session::get('msg')}} <h4>
               <br>
                <h3 class="box-title">Create a new product combination</h3>
           </div>
           
            
      <div class="box-body">
              
        <div class="row" id="root">

            <form action="{{ route('storeProductCombination') }}" method="post">

                 {{ csrf_field() }}
             
               <div class="col-sm-12">
                  <div class="form-group col-sm-6">
                    
                    <label>Product</label>
                    <select class="form-control" name="product_id" required="" v-model="addProduct">
                      @foreach($products as $data)
                       <option value="{{ $data->id }}">{{ $data->name }}</option>
                      @endforeach
                    </select>

                     @if($errors->has('product_id'))
                       <p style="color: red">Product field is required</p>
                     @endif

                  </div>
               </div>


           <div class="col-sm-12">
              <div class="form-group col-sm-6">
                <label>Size</label>

              <select  name="size_id[]" multiple="multiple" class="form-control" v-model="addSize">
                 @foreach($sizes as $data)
                       <option value="{{ $data->id }}">{{ $data->name }}</option>
                 @endforeach
             
              </select>
                  @if($errors->has('size_id'))
                     <p style="color: red">Product size field is required</p>
                  @endif

              </div>
           </div>


          <div class="col-sm-12">
              <div class="form-group col-sm-6">
                <label>Color</label>

              <select name="color_id[]" multiple="multiple" class="form-control" v-model="addColor">
                 @foreach($colors as $data)
                      <option value="{{ $data->id }}" >{{ $data->name }}</option>
                 @endforeach
             
              </select>
                  @if($errors->has('color_id'))
                     <p style="color: red">Product color field is required</p>
                  @endif

              </div>
          </div>


          <div class="col-sm-12">
              <div class="form-group col-xs-12">
                   <input type="submit" v-if="!button" class="btn btn-primary" value="Submit">
              </div>
          </div>


        </form>

   
     <div class="col-sm-12">

          <div class="form-group col-xs-12">
               <button  v-if="button" @click="button = false" class="btn btn-primary" v-on:click="addCombination">Product Combination</button>
                
           </div>

           
      <div class="col-xs-12">
          
        <div v-for="product in productData">
          <div v-if="showProduct == product.id">
              <div v-for="size in sizeData">
                  <div v-if="showSize == size.id">
            
                  @{{ size }}

              </div>
            </div>
          </div>
        </div>
             
          <div v-for="productData in productData">
            <div v-for="product in products">
              <div v-if="productData.id == product">
                <div v-for="sizeData in sizeData">
                    <div v-for="size in sizes">
                      <div v-if="sizeData.id == size">
                        <div v-for="colorData in colorData">
                          <div v-for="color in colors">
                              <div v-if="colorData.id == color">

                    
                      <table id="example1" class="table table-bordered table-striped">
                        
                         <tr>
                           <h4>Product Combination</h4>
                           <td class="callout callout-info"><span style="color: red;">Product:</span> @{{ productData.name }}</td>
                           <td class="callout callout-info"><span style="color: red;">Size:</span> @{{ sizeData.name }}</td>
                           <td class="callout callout-info"><span style="color: red;">Color:</span> @{{ colorData.name }}</td>
                         </tr>
                        
                      </table>
                    
                       
                            </div>
                          </div>
                         </div>
                      </div>
                    </div>
                </div>
              </div>
           </div>
         </div>
          
        </div>

     </div>

    </div>

  </div>

  </div>
            
 </div>
          
</div>
        
  </section>
    
</div>

<!-- main content close -->

  @stop
