 @extends('layouts.show')


 @section('title')

      <title>Admin | Product Details</title>

  @stop

 @section('content')

 <!-- main content start -->
  <div class="content-wrapper">
 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <div class="box">

        <!-- all product data -->
        <br><br>
        <div class="box-header">
            <h3 class="box-title">{{ $product->name }} Details</h3>
        </div>
           
            
        <div class="box-body">
              
              <table id="example1" class="table table-bordered table-striped">

              <thead>

                <tr>
                  <th>Product:</th>
                  <td>{{ $product->name }}</td>
                </tr>
               
               @if($product->weight == null)
                <tr>
                  <th>Quantity:</th>
                  <td>{{ $product->quantity }}</td>
                </tr>
               @endif

               @if($product->quantity == null)
               <tr>
                  <th>Weight:</th>
                  <td>{{ $product->weight }}</td>
                </tr>
              @endif

              <tr>
                <th>Category:</th>
                <td>{{ $product->category->name }}</td>
              </tr>

              <tr>
                <th>Warehouse:</th>
                <td>{{ $product->warehouse->name }}</td>
              </tr>

              <tr>
                <th>Created By:</th>
                <td>{{ $product->create_by->name }}</td>
              </tr>

               <tr>
                <th>Updated By:</th>
                @if($product->updated_by == null)
                  <td>None</td>
                @else
                 <td>{{ $product->update_by->name }}</td>
                @endif
               </tr>

            </thead>
             
          </table>

        </div>


      </div>
            
    </div>
          
    </div>
        
    </section>
    
  </div>

   @stop


