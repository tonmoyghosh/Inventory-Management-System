@extends('layouts.app')


 @section('title')

      <title>Admin | Create Product</title>

  @stop

 @section('content')

  <!-- main content start -->
  <div class="content-wrapper">
 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <div class="box">
            
            <!-- create product form -->
          
           <div class="box-header">
                <h4 style="color: green"> {{Session::get('msg')}} <h4>
               <br>
                <h3 class="box-title">Create a new product</h3>
           </div>
           
            
          <div class="box-body">
              
            <div class="row">

              <form action="{{ route('storeProduct') }}" method="post">

                 {{ csrf_field() }}
             
              <div class="col-sm-12">
                <div class="form-group col-sm-6">
                  
                  <label>Product Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Product Name:" required="">

                   @if($errors->has('name'))
                      <p style="color: red">Product name field is required</p>
                   @endif

                </div>
              </div>

             
              <div class="col-sm-12">
                <div class="form-group col-sm-6">
                  
                  <label>Product weight</label>
                  <input type="text" class="form-control" name="weight" placeholder="Product Weight:">

                </div>
              </div>
               
              <div class="col-sm-12">
                <div class="col-sm-6">
                  <label>OR</label>
                </div>
              </div>
               
              <div class="col-sm-12">
                <div class="form-group col-sm-6">
                  
                  <label>Product Quantity</label>
                  <input type="text" class="form-control" name="quantity" placeholder="Product Quantity:">

                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group col-sm-6">
                  
                  <label>Product Category</label>
                  <select class="form-control" name="category_id" required="">
                    <option value="0">Select</option>
                    @foreach($category as $data)
                       <option value="{{ $data->id }}">{{ $data->name }}</option>
                    @endforeach
                  </select>

                   @if($errors->has('category_id'))
                     <p style="color: red">Product category field is required</p>
                   @endif

                </div>
              </div>

             <div class="col-sm-12">
                <div class="form-group col-sm-6">
                  
                 <label>Warehouse</label>
                  <select class="form-control" name="warehouse_id" required="">
                    <option value="0">Select</option>
                     @foreach($warehouse as $data)
                       <option value="{{ $data->id }}">{{ $data->name }}</option>
                    @endforeach
                  </select>

                  @if($errors->has('warehouse_id'))
                     <p style="color: red">Product warehouse field is required</p>
                  @endif

                </div>
              </div>
                
              <div class="col-sm-12">
                <div class="form-group col-xs-12">
                   <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                </div>
              </div>
             
             </form>

            </div>

          </div>

        </div>
            
      </div>
          
   </div>
        
  </section>
    
</div>

<!-- main content close -->

  @stop
