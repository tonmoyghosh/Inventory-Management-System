@extends('layouts.app')


 @section('title')

      <title>Admin | Update Warehouse</title>

  @stop

 @section('content')

  <!-- main content start -->
  <div class="content-wrapper">
 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <div class="box">
            
            <!-- update warehouse form -->
          
            <div class="box-header">
                 <h4 style="color: green"> {{Session::get('msg')}} <h4>
               <br>
                <h3 class="box-title">Update warehouse</h3>
            </div>
           
            
          <div class="box-body">
              
            <div class="row">

              <form action="../update-warehouse/{{ $warehouse->id }}" method="post">

                 {{ csrf_field() }}

                <div class="form-group col-xs-12 col-sm-6">
                  <input type="text" class="form-control" name="name" value="{{ $warehouse->name }}" required="">

                   @if($errors->has('name'))
                    <br>
                    <p style="color: red">Warehouse field is required</p>
                   @endif
                </div>
                 
                
                <div class="form-group col-xs-12">
                   <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                </div>
         

              </form>



            </div>

            </div>

            <br><br><br><br>

             </div>
            
          </div>
          
        </div>
        
    </section>
    
  </div>

   <!-- main content close -->

  @stop
