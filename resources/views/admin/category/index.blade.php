 @extends('layouts.app')


 @section('title')

      <title>Admin | Category List</title>

  @stop

 @section('content')

 <!-- main content start -->
  <div class="content-wrapper">
 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <div class="box">

        <!-- all category data -->
        <br>
        <div class="box-header">
            <h4 style="color: green"> {{Session::get('msg')}} <h4>
            <h3 class="box-title">All category list</h3>
        </div>
           
            
        <div class="box-body">
              
              <table id="example1" class="table table-bordered table-striped">

              <thead>

                <tr>
                  <th>Serial No.</th>
                  <th>Category</th>
                  <th>Created By</th>
                  <th>Updated By</th>
                  <th>Action</th>
                </tr>
                
              </thead>
             
              
              @foreach($categories as $data) 

                <tr>
                  <td>{{ $serialNo++ }}</td>
                  <td>{{ $data->name }}</td>
                  <td>{{ $data->create_by->name }}</td>
                  @if($data->updated_by == null)
                    <td> None </td>
                  @else
                    <td>{{ $data->update_by->name }}</td>
                  @endif
                  <td><a href="category/{{ $data->id }}" class="btn btn-primary">Update</a> <a href="delete-category/{{ $data->id }}" class="btn btn-danger">Delete</a></td>
                </tr>

             @endforeach
          
        </table>

         </div>


             </div>
            
          </div>
          
        </div>
        
    </section>
    
  </div>

   @stop


