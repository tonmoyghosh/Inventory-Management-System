@extends('layouts.app')


 @section('title')

      <title>Admin | Create Category</title>

  @stop

 @section('content')

  <!-- main content start -->
  <div class="content-wrapper">
 
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
         
          <div class="box">
            
            <!-- create category form -->
          
            <div class="box-header">
                 <h4 style="color: green"> {{Session::get('msg')}} <h4>
               <br>
                <h3 class="box-title">Create a new category</h3>
            </div>
           
            
          <div class="box-body">
              
            <div class="row">

              <form action="{{ route('storeCategory') }}" method="post">

                 {{ csrf_field() }}

                <div class="form-group col-xs-12 col-sm-6">
                  <input type="text" class="form-control" name="name" placeholder="Category:" required="">

                   @if($errors->has('name'))
                    <br>
                    <p style="color: red">Category field is required</p>
                   @endif
                </div>
                 
                
                <div class="form-group col-xs-12">
                   <input type="submit" name="submit" class="btn btn-primary" value="Submit">
                </div>
         

              </form>



            </div>

            </div>

            <br><br><br><br>

             </div>
            
          </div>
          
        </div>
        
    </section>
    
  </div>

   <!-- main content close -->

  @stop
