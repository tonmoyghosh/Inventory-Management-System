<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
  
</head>

<body class="hold-transition skin-blue sidebar-mini">

 <div class="wrapper">
         
      
      @yield('content')

 </div>

 </body>

<script src="https://unpkg.com/vue@2.5.3/dist/vue.js"></script>


<script>
    
    var app = new Vue({

           el: '#root',

           data: {
                
                button: true,
                showProduct: [],
                showSize: [],
                addProduct: [],
                addSize: [],
                addColor: [],
                products: [],
                colors: [],
                sizes: [],
               

          },

          methods: {

             addCombination() {
                    
                    this.products.push(this.addProduct);
                    this.sizes.push(this.addSize);
                    this.colors.push(this.addColor);

                
          }
        }

        });

 </script>
</body>
</html>
