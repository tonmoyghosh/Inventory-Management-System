<!-- sidebar start -->

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN MENU</li>
   
       <!-- Dashboard -->
        <li>
          <a href="{{ route('home') }}">
            <i class="fa fa-dashboard"></i><span>Dashboard</span>
          </a>
        </li>
 
        <!-- Product Category -->
         <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i><span>Product Category</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('createCategory') }}"><i class="fa fa-circle-o"></i>Create Category</a></li>
            <li><a href="{{ route('listCategory') }}"><i class="fa fa-circle-o"></i> Category List</a></li>
          </ul>
        </li>

        <!-- Warehouse -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i><span>Warehouse</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('createWarehouse') }}"><i class="fa fa-circle-o"></i>Create Warehouse</a></li>
            <li><a href="{{ route('listWarehouse') }}"><i class="fa fa-circle-o"></i> Warehouse List</a></li>
          </ul>
        </li>

       <!-- User Role -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user-plus"></i><span>User Role</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('createUserRole') }}"><i class="fa fa-circle-o"></i>Create User Role</a></li>
            <li><a href="{{ route('userRoleList') }}"><i class="fa fa-circle-o"></i> User Role List</a></li>
          </ul>
        </li>

        <!-- Color -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-repeat"></i><span>Product Color</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('createColor') }}"><i class="fa fa-circle-o"></i>Create Color</a></li>
            <li><a href="{{ route('colorList') }}"><i class="fa fa-circle-o"></i>Color List</a></li>
          </ul>
        </li>

        <!-- Product Size -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i><span>Product Size</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('createSize') }}"><i class="fa fa-circle-o"></i>Create Size</a></li>
            <li><a href="{{ route('sizeList') }}"><i class="fa fa-circle-o"></i>Size List</a></li>
          </ul>
        </li>


        <!-- Product -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cube"></i><span>Product</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ route('createProduct') }}"><i class="fa fa-circle-o"></i>Create Product</a></li>
            <li><a href="{{ route('productList') }}"><i class="fa fa-circle-o"></i>Product List</a></li>
          </ul>
        </li>


        <!-- User -->
        <li>
          <a href="{{ route('userList') }}">
            <i class="fa fa-user"></i><span>User</span>
          </a>
        </li>
        
       </ul>
    </section>

   </aside>

  <!-- sidebar close -->
