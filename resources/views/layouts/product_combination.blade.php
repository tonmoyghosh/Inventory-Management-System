<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   @yield('title')
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- ajax link -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('assets/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition skin-blue sidebar-mini">

 <div class="wrapper">
         
         @if (!(Auth::guest()))

           @include('layouts.assets.header')

         @endif

         @include('layouts.assets.sidebar')


             @yield('content')


        @include('layouts.assets.footer')

 </div>

 </body>

<script src="https://unpkg.com/vue@2.5.3/dist/vue.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>

<script>
    
    var app = new Vue({

           el: '#root',

           data: {
                
                button: true,
                showProduct: [],
                showSize: [],
                addProduct: [],
                addSize: [],
                addColor: [],
                products: [],
                colors: [],
                sizes: [],
                productData: {!! json_encode($products->toArray()) !!},
                colorData: {!! json_encode($colors->toArray()) !!},
                sizeData: {!! json_encode($sizes->toArray()) !!}

          },

          methods: {

             addCombination() {
                    
                    this.products.push(this.addProduct);
                    this.sizes.push(this.addSize);
                    this.colors.push(this.addColor);

                  for(var i=0; i < this.addProduct.length; i++) {

                     if(this.addProduct[i] == this.products[i]) {

                         this.showProduct.push(this.addProduct[i]);

                         for(var j=0; j < this.addSize.length; j++){
                                
                             if(this.addSize[j] == this.sizes[j]) {

                                  return this.showSize.push(this.addSize[j]);
                             }
                         }
                     }
                  }
              }
          }

        });

 </script>
</body>
</html>
