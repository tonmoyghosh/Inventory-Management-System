<!DOCTYPE html>
<html>
<head>
    <title>Inventory Management System</title>
    <link href="{{ asset('assets/css/auth.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap-theme.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/css/bootstrap.css') }}" type="text/css" rel="stylesheet">
    
</head>
<body>

        <div class="container">
            
        <div class="row" style="margin-top:60px">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                
                  @yield('content')

            </div>
        </div>

        </div>  
   
   <script src="{{ asset('assets/js/jquery-3.1.0.min.js') }}"></script>
   <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>


</body>
</html>