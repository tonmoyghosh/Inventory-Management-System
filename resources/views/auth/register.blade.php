@extends('auth.layouts.app')


@section('content')

    <form method="post" action="{{ route('register') }}">

                {{ csrf_field() }}

            <fieldset>

                <h2>Sign Up</h2>

                <hr class="colorgraph">

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                   
                   <input id="name" type="text" class="form-control input-lg" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                  @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                  @endif
                            
                </div>

                 

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                    <input id="email" type="email" class="form-control input-lg" placeholder="Email Address" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                </div>


                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                     <input id="password" type="password" class="form-control input-lg" placeholder="Password" name="password" required>

                      @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                     @endif
                </div>

                <div class="form-group">
                    <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control input-lg" name="password_confirmation" required>
                </div>

               
               <hr class="colorgraph">

                <div class="row">

                     
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign Up">
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a href="{{ route('login') }}" class="btn btn-lg btn-primary btn-block">Login</a>
                    </div>
                
                </div>
            
            </fieldset>

        </form>

    @stop
