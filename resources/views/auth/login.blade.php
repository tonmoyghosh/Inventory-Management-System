@extends('auth.layouts.app')


@section('content')

    <form method="post" action="{{ route('login') }}">

             {{ csrf_field() }}

            <fieldset>

               <h2>Please Sign In</h2>

               <hr class="colorgraph">

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                    <input id="email" type="email" class="form-control input-lg" placeholder="Email Address" name="email" value="{{ old('email') }}" required autofocus>

                     @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif
                </div>

                

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                    <input id="password" type="password" class="form-control input-lg" placeholder="Password" name="password" required>

                    @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif
                    
                </div>

                 <div class="form-group">
                                
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                                
                    </div>

                    <div class="form-group">

                           <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Forgot Your Password?
                           </a>
                    </div>


               <hr class="colorgraph">
                
                <div class="row">
                    
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <input type="submit" class="btn btn-lg btn-success btn-block" value="Sign In">
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a href="{{ route('register') }}" class="btn btn-lg btn-primary btn-block">Register</a>
                    </div>

                </div>
           </fieldset>

        </form>

     @stop
